//
//  AccountDisplayTest.swift
//  ImageGridTests
//
//  Created by Roney Sampaio on 24/11/18.
//  Copyright © 2018 Roney Sampaio. All rights reserved.
//

import XCTest
@testable import ImageGrid
@testable import Alamofire


class AccountDisplayTest: XCTestCase {
    
    var tabCatViewController: TabCatViewController!
    
    
    override func setUp() {
        setupTabCatViewController()
    }
    
    
    // MARK: Set up
    func setupTabCatViewController()
    {
        let bundle = Bundle.main
        let storyboard = UIStoryboard(name: "Main", bundle: bundle)
        tabCatViewController = storyboard.instantiateViewController(withIdentifier: "catImagesStoryboard") as? TabCatViewController
        
        UIApplication.shared.keyWindow!.rootViewController = tabCatViewController

        XCTAssertNotNil(tabCatViewController.view)
    }
    
    
    // MARK: Test Reloading Table
    func testShouldDisplayFetchedOrders()
    {
        // Given
        let viewModel:[String] = ["https://cdn2.thedogapi.com/images/SyviZlqNm_640.jpg", "https://cdn2.thedogapi.com/images/SyviZlqNm_640.jpg", "https://cdn2.thedogapi.com/images/SyviZlqNm_640.jpg"]
        
        
        // When
        
        //Teste válido para tabCat e tabDog
        //tabDogViewController.updateGrid(listURLs: viewModel)
        tabCatViewController.updateGrid(listURLs: viewModel)
        RunLoop.main.run(until: Date(timeIntervalSinceNow: 0.5))
        
        // Then
        let cells = tabCatViewController.catCollectionView.visibleCells as! [CollectionViewCell]
        XCTAssertEqual(cells.count, viewModel.count, "Cells count should match array.count")
        for I in 0...cells.count - 1 {
            let cell = cells[I]
            XCTAssertEqual(cell.cellImage.image, UIImage(named: viewModel[I]), "Image should be matching")
        }
        
    }
    
    // MARK: Test Empty Data
    func testShouldDisplayEmptyFetchedOrders()
    {
        // Given
        let viewModel:[String] = []
        
        
        // When
        
        //Teste válido para tabCat e tabDog
        //tabDogViewController.updateGrid(listURLs: viewModel)
        tabCatViewController.updateGrid(listURLs: viewModel)
        RunLoop.main.run(until: Date(timeIntervalSinceNow: 0.5))
        
        // Then
        let cells = tabCatViewController.catCollectionView.visibleCells as! [CollectionViewCell]
        XCTAssertEqual(cells.count, 0, "Cells count should be zero")
    }
    
    
    // MARK: Test Number of Rows
    func testNumberOfSectionShouldEqaulNumberOfOrdersToDisplay()
    {
        // Given
        let viewModel:[String] = ["https://25.media.tumblr.com/tumblr_m8soyuL8s11qejbiro1_500.jpg", "https://24.media.tumblr.com/tumblr_m99bjtZ3AA1qejbiro1_500.jpg", "https://24.media.tumblr.com/tumblr_mbvn72EuiL1qzezhmo1_500.jpg"]
        
        // When
        
        //Teste válido para tabCat e tabDog
        //tabDogViewController.updateGrid(listURLs: viewModel)
        tabCatViewController.updateGrid(listURLs: viewModel)
        RunLoop.main.run(until: Date(timeIntervalSinceNow: 0.5))
        
        // Then
        let cells = tabCatViewController.catCollectionView.visibleCells as! [CollectionViewCell]
        XCTAssertEqual(cells.count, viewModel.count, "The number of table view rows should equal the number of orders to display")
    }
    
    
    
    override func tearDown() {
        tabCatViewController = nil
    }
    
}

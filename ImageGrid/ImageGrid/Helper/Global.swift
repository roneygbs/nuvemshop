//
//  Global.swift
//  ImageGrid
//
//  Created by Roney Sampaio on 23/11/18.
//  Copyright © 2018 Roney Sampaio. All rights reserved.
//


import UIKit

class Global: NSObject {
    
    
    // MARK: Project Config
    enum ProjectColors : String {
        case main = "#4DBECF"
    }
    
    
    enum ConfigDefaults : Int {
        case catImages = 25
        case dogImages = 50
    }
    
    
    // MARK: API Calls
    enum RestAPI : String {
        case baseCatURL = "https://api.thecatapi.com/v1/images/search?"
        case baseDogURL = "https://api.thedogapi.com/v1/images/search?"
    }
    
    
    // MARK: Params utilizados na API
    enum RestParams : String {
        
        case paramLimit = "limit=25"
        case paramAPIKey = "x-api-key"
        case APIKey = "10851b19-e0cc-40dc-8f2e-812b5439b489"
    }
    
    
    // MARK: Params utilizados na API
    enum RestResponse : String {
        
        case kURL = "url"
    }
}

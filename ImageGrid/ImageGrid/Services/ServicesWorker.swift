//
//  ServicesWorker.swift
//  ImageGrid
//
//  Created by Roney Sampaio on 23/11/18.
//  Copyright © 2018 Roney Sampaio. All rights reserved.
//

import Foundation
import Alamofire



protocol ServicesProtocol
{
    func returnRequest(data: [Dictionary <String, Any>])
    func returnError(errorMsg: String)
}



class ServicesWorker
{
    
    var services: ServicesProtocol?
    
    init(services: ServicesProtocol) {
        self.services = services
    }
    
    
    
    // MARK: Cat Call
    func callCatAPI() {
        let url: String = Global.RestAPI.baseCatURL.rawValue + Global.RestParams.paramLimit.rawValue
        makeCall(url: url)
    }
    
    
    
    //MARK: Dog Call
    func callDogAPI() {
        let url: String = Global.RestAPI.baseDogURL.rawValue + Global.RestParams.paramLimit.rawValue
        makeCall(url: url)
    }
    
    
    
    //MARK: API Default Call
    func makeCall(url: String) {
        
        let headers: HTTPHeaders = [
            Global.RestParams.paramAPIKey.rawValue:  Global.RestParams.APIKey.rawValue
        ]
        
        
        
        Alamofire.request(url, headers: headers).responseJSON { response in
            
            print("Response --> \(response)")

            switch (response.result) {
                
            case .success:
                
                if let resultJSON = response.result.value as? [Dictionary <String, Any>] {
                    self.services?.returnRequest(data: resultJSON)
                } else {
                    self.services?.returnError(errorMsg: "defaultConnectionError".localized)
                }
                break
                
                
            case .failure ( _):
                self.services?.returnError(errorMsg: "defaultConnectionError".localized)
                break
            }
        }
    }
}

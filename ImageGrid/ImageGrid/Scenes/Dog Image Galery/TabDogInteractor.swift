//
//  TabDogInteractor.swift
//  ImageGridTests
//
//  Created by Roney Sampaio on 25/11/18.
//  Copyright © 2018 Roney Sampaio. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol TabDogBusinessLogic
{
    func callAPI()
}

protocol TabDogDataStore
{
    //var default: String { get set }
}


class TabDogInteractor: TabDogBusinessLogic, TabDogDataStore, ServicesProtocol
{
    
    var presenter: TabDogPresentationLogic?
    var worker: TabDogWorker?
    
    
    // MARK: Feth Data
    func callAPI() {
        ServicesWorker(services: self).callDogAPI()
    }
    
    
    // MARK: Response Success
    func returnRequest(data: [Dictionary <String, Any>]) {
        if (presenter?.presentData(data: data))! {
            callAPI()
        }
    }
    
    
    // MARK: Response Error
    func returnError(errorMsg: String) {
        presenter?.presentError()
    }
}

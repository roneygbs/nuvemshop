//
//  TabDogPresenter.swift
//  ImageGridTests
//
//  Created by Roney Sampaio on 25/11/18.
//  Copyright © 2018 Roney Sampaio. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol TabDogPresentationLogic
{
    func presentData(data: [Dictionary <String, Any>]) -> Bool
    func presentError()
}

class TabDogPresenter: TabDogPresentationLogic
{
    
    weak var viewController: TabDogDisplayLogic?
    var listURLs: [String] = []
    
    
    
    // MARK: Present Data
    func presentData(data: [Dictionary <String, Any>]) -> Bool
    {
        
        let responseData = TabDog.Response(response: data)
        
        if (responseData.success!) {
            
            self.listURLs.append(contentsOf: responseData.listURLs!)
            
            //enviar lista completa para evitar logs de cancelamentos de tasks causadas pelos downloads assincronos das imagens
            //if pode ser removido, pois erros de carregamento são nativamente protegidos pela dependência AlamofireImage
            if ( self.listURLs.count >= Global.ConfigDefaults.dogImages.rawValue) {
                viewController?.updateGrid(listURLs: self.listURLs)
            }
            
            return self.listURLs.count < Global.ConfigDefaults.dogImages.rawValue
            
        } else {
            
            presentError()
            return false
        }
    }
    
    
    // MARK: Present Error
    func presentError() {
        UIAlertController.showAlertAttention(msg: "connectionErrorTryAgain".localized, view: viewController! as! UIViewController, callBackActionButton: {
            self.viewController?.fetchRequest()
        })
    }
}

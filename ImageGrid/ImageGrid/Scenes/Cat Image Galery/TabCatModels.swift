//
//  TabCatModels.swift
//  ImageGridTests
//
//  Created by Roney Sampaio on 24/11/18.
//  Copyright © 2018 Roney Sampaio. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

enum TabCat
{
    
    //Devido à simplicidade do exercício proposto os Models Cat e Dog são identicos e poderiam ser unificados em um Model comum
    //Eles foram mantidos separados por boas práticas, prevendo-se futura expansão de código
    
    // MARK: Post response
    struct Response
    {
        
        var success: Bool?
        var listURLs: [String]?
        
        
        init(response: [Dictionary <String, Any>]) {
            
            self.listURLs = [String]()
            
            for item in response {
                if let url = item[Global.RestResponse.kURL.rawValue] as? String {
                    self.listURLs?.append(url)
                }
            }
            
            self.success = self.listURLs?.count ?? Int() > 0
        }
    }
}

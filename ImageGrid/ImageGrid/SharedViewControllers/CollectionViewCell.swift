//
//  CollectionViewCell.swift
//  ImageGrid
//
//  Created by Roney Sampaio on 23/11/18.
//  Copyright © 2018 Roney Sampaio. All rights reserved.
//

import UIKit
import AlamofireImage

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var cellImage: UIImageView!
    
    func displayContent(imagePath: String) {
        
        let url = URL(string: imagePath)!
        cellImage.af_setImage(withURL: url)
    }
}

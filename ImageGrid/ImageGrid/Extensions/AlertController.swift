//
//  AlertController.swift
//  ImageGrid
//
//  Created by Roney Sampaio on 23/11/18.
//  Copyright © 2018 Roney Sampaio. All rights reserved.
//

import UIKit

typealias CallBackActionButton = ()  -> Void

extension UIAlertController {
    
    class func showAlertAttention(msg: String, view: UIViewController, callBackActionButton: @escaping CallBackActionButton) {
        let alert = UIAlertController(title: "titelAlertAttention".localized, message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
            callBackActionButton()
        }))
        
        view.present(alert, animated: true, completion: nil)
    }
}

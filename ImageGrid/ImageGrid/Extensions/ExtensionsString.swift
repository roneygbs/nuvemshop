//
//  String.swift
//  ImageGrid
//
//  Created by Roney Sampaio on 23/11/18.
//  Copyright © 2018 Roney Sampaio. All rights reserved.
//

import Foundation


extension String {
    
    
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}

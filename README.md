# Teste Nuvem Shop

Projeto executado utilizando Swift 4.2, target iOS 9+

- Foi utilizada a arquitetura VIP ao invés de MVC, com o objetivo de tornar o código totalmente escalonável e orientado a testes. O detalhamento completo desta arquitetura pode ser encontrado em [Clean Swift](https://clean-swift.com/clean-swift-ios-architecture/.io).

- Foram criados arquivos de localização em Português e Inglês. Todas as constantes e enuns foram inseridas em arquivo global.

- Testes unitários de UI foram adicionados.
